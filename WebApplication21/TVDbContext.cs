using Microsoft.EntityFrameworkCore;

namespace WebApplication21
{
    public class TVDbContext : DbContext
    {
        public TVDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<TV> TVs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TV>(x =>
            {
                x.ToTable("TVs");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd(); 
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}
