namespace WebApplication21
{
    public class TV
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public string Country { get; set; }
        public string Color { get; set; }
    }
}
