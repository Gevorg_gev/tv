using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication21
{
    [ApiController]
    [Route("[controller]")]
    public class TVController : ControllerBase
    {
        public TVController(TVDbContext tvDbContext)
        {

        }

        private static readonly string[] TV_Name = new[]
        {
            "Hisense 55 inch UHD Smart TV" , "Samsung Plasm 48 int Android TV" , "Samsung QHD OLED TV"
        };

        private static readonly string[] TV_Color = new[]
        {
            "Matt Black" , "Space Gray" , "Black"
        };

        private static readonly string[] TV_Country = new[]
        {
            "South Korea" , "Thailand" , "South Korea"
        };

        private TVDbContext tvDbContext;


        [HttpGet]
        public IEnumerable<TV> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new TV
            {
                Id = rng.Next(1, 5),
                Name = TV_Name[rng.Next(TV_Name.Length)],
                Color = TV_Color[rng.Next(TV_Color.Length)],
                Country = TV_Country[rng.Next(TV_Country.Length)]
            });
        }

        [HttpPost]
        public IActionResult Add([FromBody] TV tv)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            tvDbContext.TVs.Add(tv);
            tvDbContext.SaveChanges();

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Remove([FromRoute] int id)
        {
            var tvs = tvDbContext.TVs.Find(id);
            if(tvs == null)
            {
                return BadRequest("Wrong Id!\n PLease try again...");
            }

            tvDbContext.TVs.Remove(tvs);
            tvDbContext.SaveChanges();


            return Ok(); 
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] int Id)
        {
            var tvs = tvDbContext.TVs.Find(Id);
            if(tvs == null)
            {
                return BadRequest("Wrong Id!\n Please try again...");
            }

            tvDbContext.TVs.Update(tvs);
            tvDbContext.SaveChanges();

            return Ok(); 
        }
    }
}